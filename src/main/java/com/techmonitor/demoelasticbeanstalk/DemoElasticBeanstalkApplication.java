package com.techmonitor.demoelasticbeanstalk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoElasticBeanstalkApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoElasticBeanstalkApplication.class, args);
    }

}